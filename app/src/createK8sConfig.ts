import * as k8s from '@kubernetes/client-node';

export function createK8sConfig(): k8s.KubeConfig {
    const kc: k8s.KubeConfig = new k8s.KubeConfig();

    kc.loadFromDefault();

    return kc;
}